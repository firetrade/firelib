package com.funstat.sequenta;

public enum SignalType{
    SetupCount,SetupReach,SetupUnreach,Cdn,Recycling,Flip,Signal,Cancel,Expired,Deffered,Completed
}
