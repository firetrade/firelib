Typical sequenta setup:

![alt text](seqDoc.png)

Annotations explanation:

| Annotation | Description |
| ------------- |:-------------:| 
| ![alt text](setup.png)| Setup annotation denotes trend with number in circle equals to number of bars in the initial trend thrust till pullback|
| ![alt text](tdst.png)| Tdst - is support/resistance formed once setup is reached|
| ![alt text](signal13.png)| Signal 13 is most powerful setup|
| ![alt text](signal21.png)| Signal 21 you can trade sporadically with other confirmations |
| ![alt text](count8.png)| Signal cant happen without outreaching 8 countdown close|
